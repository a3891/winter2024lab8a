import java.util.Scanner;
public class TicTacToeApp {
	public static void main (String[] args) {
		
		System.out.println("Welcome to TicTacToe");
		Board board = new Board();
		Scanner scanner = new Scanner(System.in);
		boolean gameOver = false;
		int player = 1;
        Tile playerToken = Tile.X;
		
		while(!gameOver) {
			System.out.println(board);
			System.out.println("Enter the row: ");
			int row = scanner.nextInt();
			System.out.println("Enter the column: ");
			int col = scanner.nextInt();
			
			if (player == 1) {
                playerToken = Tile.X;
            } 
			else {
                playerToken = Tile.O;
            }
			
			while (!board.placeToken(row, col, playerToken)) {
                System.out.println("Invalid move! Try again.");
                System.out.println("Player " + player + ", enter the row (0-2) and column (0-2) to place your token:");
                row = scanner.nextInt();
                col = scanner.nextInt();
            }
			
			if (board.checkIfWinning(playerToken)) {
                System.out.println("Player " + player + " is the winner!");
                gameOver = true;
            } 
			else if (board.checkIfFull()) {
                System.out.println("It's a tie!");
                gameOver = true;
            } 
			else {
                player++;
                if (player > 2) {
                    player = 1;
                }
			}
		}
	}
}