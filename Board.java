public class Board {
	
	private Tile[][] grid;
	
	public Board() {
		grid = new Tile[3][3];
		for (int a = 0; a < grid.length; a++) {
			for (int b = 0; b < grid[a].length; b++) {
				grid[a][b] = Tile.BLANK;
			}
		}
	}
	
	public String toString() {
		StringBuilder represantation = new StringBuilder();
		for (Tile[] row : grid) {
			for (Tile tile : row) {
				represantation.append(tile.getName());
				represantation.append(" ");
			}
			represantation.append("\n");
		}
		return represantation.toString();
	}
	
	public boolean placeToken(int row, int col, Tile playerToken) {
		//checking if the row&column are valid
		if (!((row >= 1 && row <= 3) && (col >= 1 && col <= 3))) {
			return false;
		}
		// Checking if the position is already occupied
		if (grid[row-1][col-1] != Tile.BLANK) {
            return false;
        }
		else {
			grid[row-1][col-1] = playerToken;
			return true;
		}
	}
	
	public boolean checkIfFull() {
		for (Tile[] row : grid) {
			for (Tile tile : row) {
				if (tile == Tile.BLANK) {
					return false;
				}
			}
		}
		return true;
	}
	
	private boolean checkIfWinningHorizontal(Tile playerToken) {
		for (Tile[] row: grid) {
			if (row[0] == playerToken && row[1] == playerToken && row[2] == playerToken) {
                return true; 
            }
        }
        return false;
    }
	
	private boolean checkIfWinningVertical(Tile playerToken) {
        for (int i = 0; i < 3; i++) {
            if (grid[0][i] == playerToken && grid[1][i] == playerToken && grid[2][i] == playerToken) {
                return true; // Winning condition met vertically
            }
        }
        return false;
    }
	
	public boolean checkIfWinningDiagonal(Tile playerToken) {
        return (grid[0][0] == playerToken && grid[1][1] == playerToken && grid[2][2] == playerToken) ||
               (grid[0][2] == playerToken && grid[1][1] == playerToken && grid[2][0] == playerToken);
    }
	
	public boolean checkIfWinning(Tile playerToken) {
		return checkIfWinningHorizontal(playerToken) || checkIfWinningVertical(playerToken) ||  checkIfWinningDiagonal(playerToken);
	}

}